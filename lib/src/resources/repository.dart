import 'dart:async';
import 'package:projection/src/blocs/control_bloc.dart';
import 'package:projection/src/models/control_model.dart';
import 'package:projection/src/models/displayset_model.dart';
import 'package:projection/src/models/displaytrack_model.dart';

import 'news_api_provider.dart';

/// Provider for data source, either fetches data from API or reterives cached files.
class Repository {
  static ApiProvider apiProvider = ApiProvider();

  // Api sources
  List<Source> sources = <Source>[
    apiProvider,
  ];

  // Caches not fully implemented
  List<Cache> caches = <Cache>[
  ];

  Function(String) get setAuthToken => apiProvider.setAuthToken;

  Future<List<DisplaySetModel>> fetchDisplaySets() {
    return sources[0].fetchDisplaySets();
  }

  Future<List<DisplayTrackModel>> fetchDisplayTracksOfSet(int id) {
    return sources[0].fetchDisplayTracksOfSet(id);
  }

  Future<ControlModel> fetchControl() {
    return sources[0].fetchControl();
  }

  Future<String> users() {
    return apiProvider.getUsers();
  }

  Future<bool> login(String username, String password, bool remember) {
    return apiProvider.formLogin(username, password, remember);
  }

  Future<bool> testToken(String token) {
    return apiProvider.testToken(token);
  }

  Future<bool> testConnection() {
    return apiProvider.testConnection();
  }

///  Control Requests

  Future<bool> setDisplaySetToPlay(int displaySetId) {
    return apiProvider.setDisplaySetToPlay(displaySetId);
  }

  Future<bool> play() {
    return apiProvider.play();
  }

  Future<bool> pause() {
    return apiProvider.pause();
  }
  Future<bool> stop() {
    return apiProvider.stop();
  }
  Future<bool> forward() {
    return apiProvider.forward();
  }
  Future<bool> rewind() {
    return apiProvider.rewind();
  }
  Future<bool> loop() {
    return apiProvider.loop();
  }
  Future<bool> shuffle() {
    return apiProvider.shuffle();
  }
  Future<bool> goto(int currentDisplaySetId, int currentDisplayTrackId, int gotoDisplayTrackId) {
    return apiProvider.goto(currentDisplaySetId, currentDisplayTrackId, gotoDisplayTrackId);
  }

  Future<bool> runAutonomous(){
    return apiProvider.runAutonomous();
  }

  ///  END Control Requests


clearCache() async {
  for (var cache in caches) {
    await cache.clear();
  }
}}

abstract class Source {

  Future<ControlModel> fetchControl();

  Future<List<DisplaySetModel>> fetchDisplaySets();

  Future<List<DisplayTrackModel>> fetchDisplayTracksOfSet(int id);
}

abstract class Cache {
  Future<int> clear();
}
