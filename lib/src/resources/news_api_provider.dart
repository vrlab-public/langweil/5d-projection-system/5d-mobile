import 'dart:convert';
import 'dart:io';
import 'package:projection/src/blocs/bloc.dart';
import 'package:projection/src/models/control_model.dart';
import 'package:projection/src/models/displayset_model.dart';
import 'package:projection/src/models/displaytrack_model.dart';
import 'package:projection/src/resources/repository.dart';
import 'package:http/http.dart' show Client, Response;

/// Provider for API endpoints
class ApiProvider implements Source {
  Client client = Client();
  final String _root = Bloc.serverRoot;
  final String _api = '/api';
  final String _player = '/player';
  String authorizationToken = '';

  void setAuthToken(String token) => this.authorizationToken = token;

  Future<Response> _get(String url) {
    print('GET --> $url , Authorization: $authorizationToken');
    return client.get(url,
        headers: {"Authorization": authorizationToken}).catchError((onError) {
      print(onError);
      return null;
    });
  }

  Future<Response> _post(String url) {
    print('POST --> $url , Authorization: $authorizationToken');
    return client.post(url,
        headers: {"Authorization": authorizationToken}).catchError((onError) {
      print(onError);
      return null;
    });
  }

  Future<List<DisplaySetModel>> fetchDisplaySets() async {
    final response = await _get('$_root$_api/displaysets');
    if (response == null) return [];
    Iterable l = json.decode(response.body);
    return l.map((displaySet) => DisplaySetModel.fromJson(displaySet)).toList();
  }

  @override
  Future<List<DisplayTrackModel>> fetchDisplayTracksOfSet(int id) async {
    final response = await _get('$_root$_api/displaysets/$id/displaytracks');
    if (response == null) return [];
    Iterable l = json.decode(response.body);
    return l
        .map((displayTrack) => DisplayTrackModel.fromJson(displayTrack))
        .toList();
  }

  Future<String> getUsers() async {
    final response = _get('$_root/users');
    response.then((val) {
      if (val != null) {
        print(val.statusCode);
      }
    });
  }

  Future<bool> testToken(String token) async {
    Response response =
        await client.get('$_root$_api/test', headers: {"Authorization": token});
    if (response == null || response.statusCode != 200) return false;
    return true;
  }

  Future<bool> testConnection() async {
    Response response = await client.get('$_root$_api/test');
    print('test connection ${response.statusCode}');
    if (response == null || response.statusCode != 403) return false;

    return true;
  }

  Future<bool> formLogin(
      String username, String password, bool remember) async {
    final response = await client.post(
      '$_root/login',
      body: jsonEncode({
        'username': username,
        'password': password,
      }),
    );
    if (response == null || response.statusCode != 200) return false;
    final String token = response.headers['authorization'];
    if (token == null || !token.contains('Bearer')) return false;
    this.authorizationToken = token;
    if (remember) Bloc.bloc.writeValidAuthorizationToken(token);
    return true;
  }

//  Control events
  Future<bool> play() async {
    Response res = await _post('$_root$_api$_player/play');
    return res != null && res.statusCode == 200;
  }

  Future<bool> pause() async {
    Response res = await _post('$_root$_api$_player/pause');
    return res != null && res.statusCode == 200;
  }

  Future<bool> stop() async {
    Response res = await _post('$_root$_api$_player/stop');
    return res != null && res.statusCode == 200;
  }

  Future<bool> clear() async {
    Response res = await _post('$_root$_api$_player/clear');
    return res != null && res.statusCode == 200;
  }

  Future<bool> forward() async {
    Response res = await _post('$_root$_api$_player/next');
    return res != null && res.statusCode == 200;
  }

  Future<bool> rewind() async {
    Response res = await _post('$_root$_api$_player/next/-1');
    return res != null && res.statusCode == 200;
  }

  Future<bool> loop() async {
    Response res = await _post('$_root$_api$_player/repeat');
    return res != null && res.statusCode == 200;
  }

  Future<bool> shuffle() async {
    Response res = await _post('$_root$_api$_player/shuffle');
    return res != null && res.statusCode == 200;
  }

  Future<bool> goto(int currentDisplaySetId, int currentDisplayTrackId, int gotoDisplayTrackId)  async {
    Response res = await _post('$_root$_api$_player/goto/$currentDisplaySetId/$currentDisplayTrackId/$gotoDisplayTrackId');
    return res != null && res.statusCode == 200;
  }

  Future<bool> move(int fromIndexFromPlaylist, int toIndexFromPlaylist) async {
    Response res = await _post(
        '$_root$_api$_player/move/$fromIndexFromPlaylist/$toIndexFromPlaylist');
    return res != null && res.statusCode == 200;
  }

  Future<bool> seek(int second) async {
    Response res = await _post('$_root$_api$_player/seek/$second');
    return res != null && res.statusCode == 200;
  }

//  Future<bool> add(int displaySetId, int displayTrackId) async {
//    Response res =
//        await _post('$_root$_api$_player/add/$displaySetId/$displayTrackId');
//    return res != null && res.statusCode == 200;
//  }
//
//  Future<bool> enqueue(int displaySetId, int displayTrackId) async {
//    Response res =
//        await _post('$_root$_api$_player/enqueue/$displaySetId/$displayTrackId');
//    return res != null && res.statusCode == 200;
//  }

  Future<bool> addplaylist(int displaySetId) async {
    Response res = await _post('$_root$_api$_player/addplaylist/$displaySetId');
    return res != null && res.statusCode == 200;
  }

  Future<bool> enqueueplaylist(int displaySetId) async {
    Response res =
        await _post('$_root$_api$_player/enqueueplaylist/$displaySetId');
    return res != null && res.statusCode == 200;
  }

  Future<bool> setDisplaySetToPlay(int displaySetId) async {
    return await addplaylist(displaySetId);
  }

  @override
  Future<ControlModel> fetchControl() async {
    Response res = await _get('$_root$_api/playedtrack');
    if (res == null || res.statusCode != 200) return ControlModel.empty();
    return ControlModel.fromJson(json.decode(res.body));
  }

  Future<bool> runAutonomous() async {
    Response res = await _post('$_root$_api/runautonomous');
    return res != null && res.statusCode == 200;
  }
}
