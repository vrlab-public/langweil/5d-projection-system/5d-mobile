import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projection/src/blocs/authentication_bloc.dart';
import 'package:projection/src/blocs/bloc.dart';
import 'package:projection/src/screens/homepage_screen.dart';
import 'package:projection/src/screens/login_screen.dart';

/// Front-end Widgets configuration for splash screen
class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Timer iterateConnectTimer;
  static const int connectionEstablishInterval = 1; //seconds

  Future<bool> _checkForSession() async {
    return await Bloc.bloc.authenticationBloc.verifyAuthentication();
  }

  void _navigateToHome() {
    print('routing to Homepage');
    Navigator.of(context).pushReplacementNamed('/home');
  }

  void _navigateToLogin() {
    print('routing to Login');
    Navigator.of(context).pushReplacementNamed('/login');
  }

  @override
  void initState() {
    super.initState();
    _checkForSession().then((value) {
      if (value == null) {
        iterateConnectTimer = new Timer.periodic(
            Duration(seconds: connectionEstablishInterval),
            (Timer t) => repeat(t));
      } else {
        _navigateOn(value);
      }
    });
  }

  int repeated = 0;

  void repeat(Timer t) async {
    repeated++;
    print('hi $repeated and time $t');
    _navigateOn(await _checkForSession());
  }

  void _navigateOn(bool value) {
    if (value == null) return;
    if (iterateConnectTimer != null) iterateConnectTimer.cancel();
    if (value) {
      _navigateToHome();
    } else {
      _navigateToLogin();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: Colors.white),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/images/mhp.jpg'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    info(Bloc.bloc.authenticationBloc)
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget info(AuthenticationBloc bloc) {
    return StreamBuilder(
      stream: bloc.authStream,
      builder: (context, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasError) {
          return Text(snapshot.error);
        }
        if (!snapshot.hasData) return SizedBox.shrink();
        return snapshot.data
            ? Text('Logged in')
            : Text('Invalid username or password');
      },
    );
  }
}
