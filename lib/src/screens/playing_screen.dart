import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projection/src/blocs/bloc.dart';
import 'package:projection/src/blocs/control_bloc.dart';
import 'package:projection/src/blocs/control_provider.dart';
import 'package:projection/src/blocs/display_item_provider.dart';
import 'package:projection/src/blocs/display_items_bloc.dart';
import 'package:projection/src/models/displayset_model.dart';
import 'package:projection/src/models/displaytrack_model.dart';
import 'package:projection/src/screens/splash_screen.dart';
import 'package:projection/src/utils/colors.dart';
import 'package:projection/src/utils/text_utils.dart';
import 'package:projection/src/utils/themes.dart';
import 'package:projection/src/widgets/refresh.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import '../utils/decorators.dart';
import '../widgets/control_container.dart';

import 'login_screen.dart';

/// Front-end Widgets configuration for playback screen
class PlayScreen extends StatelessWidget {
  final DisplaySetModel displaySet;

  const PlayScreen({Key key, this.displaySet}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final displayItemBloc = DisplayItemProvider.of(context);
    final controlBloc = ControlProvider.of(context);
    return WillPopScope(
      onWillPop: () => _onBackPressed(context, controlBloc),
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: Text('Playing ${displaySet.name}'),
          flexibleSpace: Container(
            decoration: Decorators.appBar(Theme.of(context)),
          ),
        ),
        body: SlidingUpPanel(
          backdropEnabled: true,
          parallaxEnabled: true,
          minHeight: 90.0,
          panel: Container(
            alignment: Alignment.topCenter,
            decoration: Decorators.playPanel(Theme.of(context)),
            child: Column(
              children: <Widget>[
                ControlContainer(),
                Container(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                Container(
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                    'This is space for additional functionality if needed during playback',
                    style: Theme.of(context).textTheme.body1,
                  ),
                ),
                displaySet.description.isEmpty
                    ? SizedBox.shrink()
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Icon(
                            CupertinoIcons.group,
                          ),
                          Text(
                            displaySet.description,
                            style: Theme.of(context).textTheme.body1,
                          ),
                        ],
                      ),
              ],
            ),
          ),
          body: buildDisplayTracks(context, displaySet , displayItemBloc, controlBloc),
        ),
      ),
    );
  }

  buildDisplayTracks(BuildContext context, DisplaySetModel displaySet, DisplayItemBloc displayBloc,
      ControlBloc controlBloc) {
    return StreamBuilder(
        stream: displayBloc.displayTracks,
        builder: (context, AsyncSnapshot<List<DisplayTrackModel>> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            if (snapshot.data.length == 0) {
              return Center(
                child: Text('No tracks for selected set'),
              );
            }
            //display tracks loaded -> create playlist
            controlBloc.updatePlaylist(snapshot.data);
            return StreamBuilder(
                stream: controlBloc.playlist,
                builder: (context,
                    AsyncSnapshot<List<DisplayTrackModel>> snapshotPlaylist) {
                  if (!snapshotPlaylist.hasData) {
                    return Container(
                      margin: EdgeInsets.all(20.0),
                      child: CircularProgressIndicator(),
                    );
                  } else {
                    return ListView(
                      padding: EdgeInsets.only(bottom: 90.0),
                      children: <Widget>[
                        for (int i = 0; i < snapshotPlaylist.data.length; i++)
                          buildTile(
                              snapshotPlaylist.data[i], displaySet.id, context, controlBloc)
                      ],
                    );
                  }
                });
          }
        });
  }

  Widget buildTile(DisplayTrackModel model, int displaySetId, BuildContext context,
      ControlBloc controlBloc) {
    print('build ${model.name} with key ${ValueKey(model.id)}');
    return Container(
      height: 70,
      key: ValueKey(model.id),
      color: model.id != controlBloc.runningDisplayTrackId
          ? Theme.of(context).backgroundColor
          : Colors.lightGreen[800],
      margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: ListTile(
        onTap: () {
          controlBloc.setControlEvent(ControlEvent.goto,
              arguments: [displaySetId, controlBloc.runningDisplayTrackId, model.id]);
        },
        leading: SizedBox(
          width: 40,
          child: Container(
            alignment: Alignment.center,
            child: FittedBox(
              child: Row(
                  children:
                      model.type.map((type) => ofTrackType(type)).toList()),
            ),
          ),
        ),
        title: Text(model.name),
        subtitle: Text(model.description),
        trailing: Text(TextUtils.timeFormatter(double.parse(model.duration))),
      ),
    );
  }

  Future<bool> _onBackPressed(BuildContext context, ControlBloc controlBloc) {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit the playing'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: Text("NO"),
              ),
              new FlatButton(
                onPressed: () {
                  controlBloc.stopIntervalQuerying();
                  controlBloc.setControlEvent(ControlEvent.stop);
                  controlBloc.isLoop = false;
                  controlBloc.isShuffle = false;
                  if (Bloc.bloc.defaultTheme == ThemeType.light)
                    Bloc.bloc.setCurrentTheme(ThemeType.light);
                  Navigator.of(context).pop(true);
                },
                child: Text("YES"),
              ),
            ],
          ),
        ) ??
        false;
  }

  void _handleReorder(int oldIndex, int newIndex, List<DisplayTrackModel> data,
      ControlBloc controlBloc) {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    final DisplayTrackModel item = data.removeAt(oldIndex);
    data.insert(newIndex, item);
    print('$oldIndex -> $newIndex');
    controlBloc.updatePlaylist(data);
  }

  Icon ofTrackType(TrackType type) {
    switch (type) {
      case TrackType.video:
        return Icon(Icons.video_library);
        break;
      case TrackType.image:
        return Icon(Icons.image);
        break;
      case TrackType.audio:
        return Icon(Icons.audiotrack);
        break;
      case TrackType.unknown:
        return Icon(Icons.aspect_ratio);
        break;
    }
  }
}
