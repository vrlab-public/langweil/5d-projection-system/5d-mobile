import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projection/src/blocs/authentication_bloc.dart';
import 'package:projection/src/blocs/bloc.dart';

import 'homepage_screen.dart';

/// Front-end Widgets configuration for login screen
class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authenticationBloc = Bloc.bloc.authenticationBloc;
    authenticationBloc.initAuth();
    authenticationBloc.authStream.listen((onData) {
      print('auth $onData');
      if (!onData) return;
      Navigator.of(context).pushReplacementNamed('/home');
    });
    return Scaffold(
      body: buildLogin(authenticationBloc, context),
    );
  }

  Widget buildLogin(AuthenticationBloc bloc, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          logo(context),
          info(bloc),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              usernameField(bloc),
              passwordField(bloc),
              checkBox(bloc),
            ],
          ),
          spinner(bloc),
          submitButton(bloc),
        ],
      ),
    );
  }

  Widget logo(BuildContext context) {
    return Column(
      children: <Widget>[
        Image(
          image: AssetImage('assets/images/museumLogo.png'),
          width: 100.0,
        ),
        Padding(
          padding: EdgeInsets.only(top: 20.0),
        ),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: 'Langweilův model Prahy',
            style: Theme.of(context).textTheme.title,
          ),
        )
      ],
    );
  }

  Widget info(AuthenticationBloc bloc) {
    return StreamBuilder(
      stream: bloc.authStream,
      builder: (context, AsyncSnapshot<bool> snapshot) {
        print('has error ${snapshot.hasError}');
        if (snapshot.hasError) {
          return Text(snapshot.error);
        }
        if (!snapshot.hasData) return SizedBox.shrink();
        return snapshot.data
            ? Text('Logged in')
            : Text('Invalid username or password');
      },
    );
  }

  Widget usernameField(AuthenticationBloc bloc) {
    return StreamBuilder(
      stream: bloc.username,
      builder: (context, snapshot) {
        print('username data ${snapshot.data}');
        return TextField(
          onChanged: bloc.changeUsername,
          keyboardType: TextInputType.text,
          decoration: InputDecoration(
            hintText: 'username',
            labelText: 'Enter username',
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

  Widget passwordField(AuthenticationBloc bloc) {
    return StreamBuilder(
      stream: bloc.password,
      builder: (context, snapshot) {
        print('password data ${snapshot.data}');
        return TextField(
          onChanged: bloc.changePassword,
          obscureText: true,
          decoration: InputDecoration(
            hintText: 'password',
            labelText: 'Password',
            errorText: snapshot.error,
          ),
        );
      },
    );
  }

  Widget spinner(AuthenticationBloc bloc) {
    return StreamBuilder(
        stream: bloc.isValidating,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          return snapshot.data == null || !snapshot.data
              ? SizedBox.shrink()
              : Center(
                  child: CircularProgressIndicator(),
                );
        });
  }

  Widget submitButton(AuthenticationBloc bloc) {
    return StreamBuilder(
      stream: bloc.submitValid,
      builder: (context, AsyncSnapshot<bool> snapshot) {
        print('submit data ${snapshot.data}');
        return ButtonTheme(
          minWidth: double.infinity,
          child: RaisedButton(
            child: Text('Login'),
            disabledColor: CupertinoColors.inactiveGray,
            color: Colors.blue,
            onPressed:
                (snapshot.hasError || !snapshot.hasData) ? null : bloc.submit,
          ),
        );
      },
    );
  }

  Widget checkBox(AuthenticationBloc bloc) {
    return StreamBuilder(
      stream: bloc.rememberMe,
      builder: (context, AsyncSnapshot<bool> snapshot) {
        print('check data ${snapshot.data}');
        return Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              'Remember me',
              style: Theme.of(context).textTheme.body1,
            ),
            Checkbox(
              value: snapshot.data ?? false,
              onChanged: (bool val) => bloc.changeRememberMe(val),
            ),
          ],
        );
      },
    );
  }
}
