import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projection/src/blocs/bloc.dart';
import 'package:projection/src/blocs/display_item_provider.dart';
import 'package:projection/src/blocs/display_items_bloc.dart';
import 'package:projection/src/models/displayset_model.dart';
import 'package:projection/src/models/displaytrack_model.dart';
import 'package:projection/src/screens/splash_screen.dart';
import 'package:projection/src/utils/colors.dart';
import 'package:projection/src/utils/text_utils.dart';
import 'package:projection/src/utils/themes.dart';
import 'package:projection/src/widgets/refresh.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import '../utils/decorators.dart';
import '../widgets/control_container.dart';

import 'login_screen.dart';

/// Front-end Widgets configuration for displayTrackScreen
class DisplaytrackScreen extends StatelessWidget {
  final DisplaySetModel displaySet;

  DisplaytrackScreen({this.displaySet});

  @override
  Widget build(BuildContext context) {
    final displayItemBloc = DisplayItemProvider.of(context);
    return Scaffold(
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      appBar: AppBar(
        title: Text('Display tracks'),
        flexibleSpace: Container(
          decoration: Decorators.appBar(Theme.of(context)),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          buildDisplayTracks(context, displayItemBloc),
          Container(
            decoration: Decorators.displayTrackCard(Theme.of(context)),
            width: double.infinity,
            height: 200,
            child: ListView(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: Container(
                        child: Text(
                          displaySet.name,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 26.0,
                          ),
                          overflow: TextOverflow.fade,
                        ),
                        padding: EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 20.0, bottom: 10.0),
                      ),
                    ),
                    SizedBox(
                      width: 100,
//                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10.0),
                        child: StreamBuilder(
                          stream: displayItemBloc.displayTracks,
                          builder: (context, snapshot) {
                            return RaisedButton(
                                color: Bloc.bloc.currentTheme == ThemeType.light
                                    ? Colors.greenAccent[400]
                                    : Colors.green,
                                onPressed: snapshot.data.length == 0 ? null : () {
                                  Bloc.bloc.setCurrentTheme(ThemeType.dark);
                                  Navigator.pushNamed(context, '/play',
                                      arguments: <String, DisplaySetModel>{
                                        'displaySet': displaySet,
                                      });
                                },
                                child: Text('Start'),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Flexible(
                      child: Container(
                        alignment: Alignment.topLeft,
                        padding: EdgeInsets.only(
                            left: 20.0, right: 20.0, bottom: 10.0),
                        child: Text(
                          displaySet.description,
                          overflow: TextOverflow.visible,
                          style: TextStyle(
//                    fontWeight: FontWeight.bold,
//                    fontSize: 26.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  buildDisplayTracks(BuildContext context, DisplayItemBloc displayBloc) {
    return StreamBuilder(
        stream: displayBloc.displayTracks,
        builder: (context, AsyncSnapshot<List<DisplayTrackModel>> snapshot) {
          if (!snapshot.hasData) {
            return Container(
              margin: EdgeInsets.all(20.0),
              child: CircularProgressIndicator(),
            );
          } else {
            if (snapshot.data.length == 0) {
              return Center(
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  child: Text('No tracks for selected set'),
                ),
              );
            }
            print(snapshot.data.length);
            return Expanded(
              child: ListView.builder(
                  padding: EdgeInsets.only(bottom: 10.0),
//                shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, int index) {
                    return buildTile(snapshot.data[index], context);
                  }),
            );
          }
        });
  }

  Widget buildTile(DisplayTrackModel model, BuildContext context) {
    print('build ${model.name}');
    return Container(
      decoration: Decorators.displayTrackTile(Theme.of(context)),
      margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
      child: ListTile(
//            contentPadding: EdgeInsets.all(10.0),
        onTap: () {
          print('tapped track');
        },
        title: Text(model.name),
        subtitle: Text(model.description),
        trailing: Text(TextUtils.timeFormatter(double.parse(model.duration))),
      ),
    );
//        Divider(
//          height: 8.0,
//        ),
//      ],
//    );
  }
}
