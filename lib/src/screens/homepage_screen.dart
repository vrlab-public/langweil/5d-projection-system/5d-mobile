import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projection/src/blocs/bloc.dart';
import 'package:projection/src/blocs/control_bloc.dart';
import 'package:projection/src/blocs/control_provider.dart';
import 'package:projection/src/blocs/display_items_bloc.dart';
import 'package:projection/src/models/displayset_model.dart';
import 'package:projection/src/screens/splash_screen.dart';
import 'package:projection/src/utils/colors.dart';
import 'package:projection/src/utils/text_utils.dart';
import 'package:projection/src/utils/themes.dart';
import 'package:projection/src/widgets/side_menu.dart';
import '../utils/decorators.dart';
import '../blocs/display_item_provider.dart';

import 'login_screen.dart';

/// Front-end Widgets configuration for homepage screen
class HomepageScreen extends StatelessWidget {
  final bloc = Bloc.bloc;

  @override
  Widget build(BuildContext context) {
    final displayItemBloc = DisplayItemProvider.of(context);
    return Scaffold(
      drawer: SideMenu(),
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      appBar: AppBar(
        title: Text('Homepage'),
        flexibleSpace: Container(
          decoration: Decorators.appBar(Theme.of(context)),
        ),
      ),
      body: buildHomepage(context, displayItemBloc),
    );
  }

  void handleClick(String value, BuildContext context) {
    print(value);
    switch (value) {
      case 'Logout':
        bloc.authenticationBloc.logout();
        Navigator.of(context).pushReplacementNamed('/login');
        break;
      case 'Settings':
        break;
      default:
        break;
    }
  }

  buildHomepage(BuildContext context, DisplayItemBloc displayItemBloc) {
    ControlBloc controlBloc = ControlProvider.of(context);
    return StreamBuilder(
        stream: displayItemBloc.displaySets,
        builder: (context, AsyncSnapshot<List<DisplaySetModel>> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Column(
              children: <Widget>[
                Expanded(
                  child: GridView.count(
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    padding: const EdgeInsets.all(10),
                    mainAxisSpacing: 10,
                    crossAxisSpacing: 10,
                    children: _buildGridTileList(snapshot.data, context),
                  ),
                ),
                Container(
                  decoration: Decorators.displayTrackCard(Theme.of(context)),
                  height: 80,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text('Or run prepared autonomous mode',style: Theme.of(context).textTheme.body2,),
                          RaisedButton(
                            child: Text('Run autonomous'),
                            disabledColor: CupertinoColors.inactiveGray,
                            color: Colors.blue,
                            onPressed: () {
                              controlBloc.runAutonomous();
                            },
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            );
          }
        });
  }

  List<Container> _buildGridTileList(List<DisplaySetModel> list,
      BuildContext context) =>
      List.generate(list.length, (i) => buildSquare(list[i], context));

  buildSquare(DisplaySetModel displaySet, BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          print('tapped');
          Navigator.pushNamed(context, '/displaytrack',
              arguments: <String, DisplaySetModel>{
                'displaySet': displaySet,
              });
        },
        child: GridTile(
          child: SizedBox(
            width: 42.0,
            height: 42.0,
            child: DecoratedBox(
              decoration: Decorators.displaySetTile(Theme.of(context)),
            ),
          ),
          footer: GridTileBar(
//            trailing: Text(displaySet.createdByUsername),
//            backgroundColor: Colors.,
            title: Text(
              displaySet.name,
              style: Theme
                  .of(context)
                  .textTheme
                  .title,
            ),
            subtitle: Text(
              '${TextUtils.timeFormatter(
                  double.parse(displaySet.duration))} min',
              style: Theme
                  .of(context)
                  .textTheme
                  .subhead,
            ),
//
          ),
        ),
      ),
    );
//    ,
//    );
  }
}
