import 'package:flutter/material.dart';
import 'package:projection/src/models/displayset_model.dart';
import 'package:projection/src/screens/displaytrack_screen.dart';
import 'package:projection/src/screens/playing_screen.dart';
import 'blocs/display_item_provider.dart';
import 'blocs/control_provider.dart';
import 'screens/splash_screen.dart';
import 'blocs/bloc.dart';
import 'screens/homepage_screen.dart';
import 'screens/login_screen.dart';
import 'utils/colors.dart';
import 'utils/themes.dart';

/// Centralized class representing the application
class App extends StatelessWidget {
  final bloc = Bloc.bloc;

  @override
  Widget build(BuildContext context) {
    return ControlProvider(
      child: DisplayItemProvider(
        child: StreamBuilder(
          stream: bloc.themeStream,
          builder: (context, AsyncSnapshot<ThemeType> snapshot) {
            return MaterialApp(
              theme: !snapshot.hasData || snapshot.hasError
                  ? ThemeProvider.light
                  : snapshot.data == ThemeType.light
                      ? ThemeProvider.light
                      : ThemeProvider.dark,
              title: 'Projection',
              onGenerateRoute: routes,
            );
          },
        ),
      ),
    );
  }

  /// Integrated routing method
  Route routes(RouteSettings setting) {
    print(
        'routing to ${setting.name} and isInitialized: ${bloc.isInitialized} and Authenticated ${bloc.isAuthenticated}');

    if (!bloc.isInitialized) {
      bloc.isInitialized = true;
      return MaterialPageRoute(builder: (context) => SplashScreen());
    }

    switch (setting.name) {
      case '/login':
        return MaterialPageRoute(
          builder: (context) => LoginScreen(),
        );

      case '/':
      case '/home':
        return MaterialPageRoute(builder: (context) {
          final displayItemBloc = DisplayItemProvider.of(context);
          displayItemBloc.fetchDisplaySets();
          return HomepageScreen();
        });

      case '/displaytrack':
        return MaterialPageRoute(builder: (context) {
          final displayItemBloc = DisplayItemProvider.of(context);
          final DisplaySetModel displaySet =
              (setting.arguments as Map<String, DisplaySetModel>)['displaySet'];
          displayItemBloc.fetchDisplayTracksOfDisplaySet(displaySet.id);
          return DisplaytrackScreen(
            displaySet: displaySet,
          );
        });

      case '/play':
        return MaterialPageRoute(builder: (context) {
          final displayItemBloc = DisplayItemProvider.of(context);
          final DisplaySetModel displaySet =
              (setting.arguments as Map<String, DisplaySetModel>)['displaySet'];
          displayItemBloc.fetchDisplayTracksOfDisplaySet(displaySet.id);
          final controlBloc = ControlProvider.of(context);
          controlBloc.setDisplaySetToPlay(displaySet.id);
          controlBloc.startIntervalQuerying();
          return PlayScreen(
            displaySet: displaySet,
          );
        });
      default:
        return null;
    }
  }
}
