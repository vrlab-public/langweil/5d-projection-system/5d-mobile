import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:projection/src/blocs/bloc.dart';
import 'package:projection/src/utils/themes.dart';
import 'package:rxdart/rxdart.dart';
import 'package:url_launcher/url_launcher.dart';

/// Excluded widget representing side-menu
class SideMenu extends StatelessWidget {
  final _toggleStream = BehaviorSubject<ThemeType>();
  Bloc bloc = Bloc.bloc;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment:MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                  child: Text(
                    'Menu',
                    style: TextStyle(color: Colors.white, fontSize: 25),
                  ),
                  decoration: BoxDecoration(
                    color: Colors.grey,
                  ),
                ),
                ListTile(
                  leading: Icon(CupertinoIcons.share_up),
                  title: Text('Administrator site'),
                  onTap: () async {
                    const url = Bloc.serverRoot;
                    if (await canLaunch(url)) launch(url);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.exit_to_app),
                  title: Text('Logout'),
                  onTap: () {
                    bloc.authenticationBloc.logout();
                    Navigator.of(context).pushReplacementNamed('/login');
                  },
                ),
              ],
            ),
          ),
          StreamBuilder(
              stream: _toggleStream,
              builder: (context, AsyncSnapshot<ThemeType> snapshot) {
                return Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text('Theme'),
                      Padding(
                        padding: const EdgeInsets.only(left: 10.0),
                        child: ToggleButtons(
                          borderRadius: BorderRadius.circular(8.0),
                          children: <Widget>[
                            Text('light'),
                            Text('dark'),
                          ],
                          isSelected: List.of(Bloc.bloc.defaultTheme == ThemeType.dark
                              ? [false, true]
                              : [true, false]),
                          onPressed: (int index) {
                            bloc.setDefaultTheme(convert(Bloc.bloc.defaultTheme));
                            _toggleStream.sink.add(convert(Bloc.bloc.defaultTheme));
                          },
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ],
      ),
    );
  }

  ThemeType convert(ThemeType type) {
    return type == ThemeType.light ? ThemeType.dark : ThemeType.light;
  }

  dispose() {
    _toggleStream.close();
  }
}
