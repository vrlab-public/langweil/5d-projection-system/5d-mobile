import 'package:flutter/material.dart';
import 'package:projection/src/blocs/control_provider.dart';

/// Cache invalidation and new data retrieval mechanism
class Refresh extends StatelessWidget {
  final Widget child;

  const Refresh({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = ControlProvider.of(context);
    return RefreshIndicator(
      child: child,
      onRefresh: () async {
//        await bloc.clearCache();
//        await bloc.fetchNewData();
      },
    );
  }
}
