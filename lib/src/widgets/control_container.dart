import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:projection/src/blocs/control_bloc.dart';
import 'package:projection/src/blocs/control_provider.dart';
import 'package:projection/src/models/control_model.dart';
import 'package:projection/src/utils/colors.dart';

/// Excluded complex widget for playback control
class ControlContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return buildContainer(context);
  }

  Widget buildContainer(BuildContext context) {
    ControlBloc controlBloc = ControlProvider.of(context);
    print('controlBloc $controlBloc');
    Color iconColor = Theme.of(context).iconTheme.color;
    return StreamBuilder(
        stream: controlBloc.playControl,
        builder: (context, AsyncSnapshot<ControlModel> snapshot) {
          print('draw Play control');
          if (snapshot.hasError || !snapshot.hasData) return SizedBox.shrink();
          print('${controlBloc.isPlaying} vs ${snapshot.data.isPlaying}');
          return Container(
            height: 90.0,
            color: CustomColors.semiBlack,
            child: Material(
              color: CustomColors.semiBlack,
              child: Column(
                children: <Widget>[
                  LinearProgressIndicator(
                    backgroundColor: Colors.grey[800],
                    value: (snapshot.data.trackProgress / 100),
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 6.0),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Text(
                          snapshot.data.trackTimeElapsed,
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Text(
                          snapshot.data.trackDuration,
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          CupertinoIcons.shuffle,
                          color: controlBloc.isShuffle
                              ? Colors.lightGreen[400]
                              : Colors.white,
                        ),
                        onPressed: null,
//                        onPressed: () {
//                          print('shuffle');
//                          controlBloc.setControlEvent(ControlEvent.shuffle);
//                        },
                      ),
                      IconButton(
                        iconSize: 36.0,
                        icon: Icon(
                          Icons.skip_previous,
                        ),
                        onPressed: () {
                          print('prev');
                          controlBloc.setControlEvent(ControlEvent.rewind);
                        },
                      ),
                      controlBloc.isPlaying
                          ? IconButton(
                              splashColor: Colors.red,
                              iconSize: 36.0,
                              icon: Icon(
                                Icons.pause,
                                color: iconColor,
                              ),
                              onPressed: () {
                                print('pause');
                                controlBloc.setControlEvent(ControlEvent.pause);
                              },
                            )
                          : IconButton(
                              splashColor: Colors.lightGreenAccent,
                              iconSize: 36.0,
                              icon: Icon(
                                Icons.play_arrow,
                                color: iconColor,
                              ),
                              onPressed: () {
                                print('play');
                                controlBloc.setControlEvent(ControlEvent.play);
                              },
                            ),
                      IconButton(
                        iconSize: 36.0,
                        icon: Icon(
                          Icons.skip_next,
                        ),
                        onPressed: () {
                          print('forward');
                          controlBloc.setControlEvent(ControlEvent.forward);
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          CupertinoIcons.loop_thick,
                          color: controlBloc.isLoop
                              ? Colors.lightGreen[400]
                              : Colors.white,
                        ),
                        onPressed: () {
                          print('loop');
                          controlBloc.setControlEvent(ControlEvent.loop);
                        },
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
}
