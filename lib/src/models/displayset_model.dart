import 'dart:convert';


/// Model for displaySet transfer object
class DisplaySetModel {
  final int id;
  final String name;
  final String createdByUsername;
  final String createdCet;
  final bool published;
  final bool setAsAutonomous;
  final String duration;
  final String description;

  DisplaySetModel.fromJson(Map<String, dynamic> parsedJson)
      : id = int.parse(parsedJson['id']),
        name = parsedJson['name'] ?? false,
        createdByUsername = parsedJson['createdByUsername'],
        createdCet = parsedJson['createdCet'] ?? '',
        published = parsedJson['published'].toString().toLowerCase() == 'true',
        setAsAutonomous =
            parsedJson['setAsAutonomous'].toString().toLowerCase() == 'true',
        duration = parsedJson['duration'],
        description =parsedJson['description'];

  DisplaySetModel.fromDb(Map<String, dynamic> parsedJson)
      : id = parsedJson['id'],
        name = parsedJson['name'],
        createdByUsername = parsedJson['createdByUsername'],
        createdCet = parsedJson['createdCet'],
        published = parsedJson['published'],
        setAsAutonomous = parsedJson['setAsAutonomous'],
        duration = parsedJson['duration'],
        description =parsedJson['description'];

  Map<String, dynamic> toMapForDb() {
    return <String, dynamic>{
      "id": id,
      "name": name,
      "createdByUsername": createdByUsername,
      "createdCet": createdCet,
      "published": published,
      "setAsAutonomous": setAsAutonomous,
      "duration" : duration,
      "description" : description
    };
  }
}
