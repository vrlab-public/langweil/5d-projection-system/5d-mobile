import 'dart:convert';

/// Enum represents displayTrack type
enum TrackType {
  video,
  image,
  audio,
  unknown,
}

/// Model for displayTrack transfer object
class DisplayTrackModel {
  final int id;
  final String name;
  final String displaySetName;
  final String createdByUsername;
  final String createdCet;
  final List<TrackType> type;
  final String duration;
  final String description;
  bool isPlaying = false;

  DisplayTrackModel.fromJson(Map<String, dynamic> parsedJson)
      : id = int.parse(parsedJson['id']),
        name = parsedJson['name'] ?? false,
        createdByUsername = parsedJson['createdByUsername'],
        createdCet = parsedJson['createdCet'] ?? '',
        displaySetName = parsedJson['displaySet'],
        type = parsedJson['trackType']
            .toString()
            .split(",")
            .map((type) => getType(type))
            .toList(),
        duration = parsedJson['duration'],
        description = parsedJson['description'];

  DisplayTrackModel.fromDb(Map<String, dynamic> parsedJson)
      : id = parsedJson['id'],
        name = parsedJson['name'],
        createdByUsername = parsedJson['createdByUsername'],
        createdCet = parsedJson['createdCet'],
        displaySetName = parsedJson['displaySetName'],
        type = jsonDecode(parsedJson['type']),
        duration = parsedJson['duration'],
        description = parsedJson['description'];

  Map<String, dynamic> toMapForDb() {
    return <String, dynamic>{
      "id": id,
      "name": name,
      "createdByUsername": createdByUsername,
      "createdCet": createdCet,
      "displaySetName": displaySetName,
      "type": jsonEncode(type),
      "duration": duration,
      "description": description
    };
  }

  static TrackType getType(String type) {
    switch (type.toLowerCase()) {
      case 'video':
        return TrackType.video;
      case 'image':
        return TrackType.image;
      case 'audio':
        return TrackType.audio;
      default:
        return TrackType.unknown;
    }
  }
}
