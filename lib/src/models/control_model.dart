import 'dart:convert';

/// Model for playback control track object
class Track {
  final int index;
  final String name;
  final int displayTrackId;
  final bool running;

  Track({this.index, this.name, this.displayTrackId, this.running});
}
/// Model for playback control transfer object
class ControlModel {
  final bool isPlaying;
  final String playingTrackName;
  final String trackTimeElapsed;
  final String trackDuration;
  final String displaySet;
  final String displayTrack;
  final int currentTime;
  final int length;
  final List<Track> playlistFromClient;
  final int trackProgress; //0 -> 100

  static ControlModel empty() {
    return ControlModel.fromJson(<String, dynamic>{
      'title': '',
      'currentTimeFormated': '--:--',
      'lengthTimeFormated': '--:--',
      'displaySet': '',
      'displayTrack': '',
      'currentTime': 0,
      'length': 0,
      'percentage': 0,
      'playlist': [],
      'playing': false
    });
  }

  static List<Track> parsePlaylist(List<dynamic> input) {
    if (input == null || input.length <= 0) return [];
    return input.map((track) => parseTrack(track)).toList();
  }

  static Track parseTrack(Map<String, dynamic> track) {
    return new Track(
        index: track['index'],
        name: track['displayTrackName'],
        displayTrackId: int.parse(track['displayTrackId']),
        running: track['running'].toString().toLowerCase() == 'true');
  }

  ControlModel.fromJson(Map<String, dynamic> parsedJson)
      : isPlaying = parsedJson['playing'].toString().toLowerCase() == 'true',
        playingTrackName = parsedJson['title'],
        trackTimeElapsed = parsedJson['currentTimeFormated'],
        trackDuration = parsedJson['lengthTimeFormated'],
        displaySet = parsedJson['displaySet'],
        displayTrack = parsedJson['displayTrack'],
        currentTime = parsedJson['currentTime'],
        length = parsedJson['length'],
        playlistFromClient = parsePlaylist(parsedJson['playlist']) ?? [],
        trackProgress = parsedJson['percentage'];

  ControlModel.fromDb(Map<String, dynamic> parsedJson)
      : isPlaying = parsedJson['isPlaying'] == 1,
        playingTrackName = parsedJson['playingTrackName'],
        trackTimeElapsed = parsedJson['trackTimeElapsed'],
        trackDuration = parsedJson['trackDuration'],
        displaySet = parsedJson['displaySet'],
        displayTrack = parsedJson['displayTrack'],
        currentTime = parsedJson['currentTime'],
        length = parsedJson['length'],
        playlistFromClient = jsonDecode(parsedJson['playlistFromClient']),
        trackProgress = parsedJson['trackProgress'];

  Map<String, dynamic> toMapForDb() {
    return <String, dynamic>{
      "isPlaying": isPlaying ? 1 : 0,
      "playingTrackName": playingTrackName,
      "trackTimeElapsed": trackTimeElapsed,
      "trackDuration": trackDuration,
      "displaySet": displaySet,
      "displayTrack": displayTrack,
      "currentTime": currentTime,
      "length": length,
      "playlistFromClient": jsonEncode(playlistFromClient),
      "trackProgress": trackProgress,
    };
  }
}
