import 'package:flutter/material.dart';
import 'package:projection/src/blocs/bloc.dart';
import 'package:projection/src/utils/colors.dart';
import 'package:projection/src/utils/themes.dart';

/// Static decorators reference - assign style of widgets across the application
class Decorators {
  static BoxDecoration appBar(ThemeData theme) {
    if (Bloc.bloc.currentTheme == ThemeType.light) {
      return BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: <Color>[Colors.blue[700], Colors.blue[400]]),
      );
    } else {
      return BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: <Color>[theme.primaryColor, CustomColors.background]),
      );
    }
  }

  static BoxDecoration displaySetTile(ThemeData theme) {
    return BoxDecoration(
      boxShadow: Bloc.bloc.currentTheme == ThemeType.light
          ? [
              BoxShadow(
                color: Colors.grey.withOpacity(0.95),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(0, 1), //
              )
            ]
          : [],
//      borderRadius: new BorderRadius.all(Radius.circular(8.0)),
      color: Bloc.bloc.currentTheme == ThemeType.light
          ? Colors.white
          : CustomColors.semiBlack,
//        gradient: LinearGradient(
//          begin: Alignment.topLeft,
//          end: Alignment.bottomRight,
//          // 10% of the width, so there are ten blinds.
//          colors: Bloc.bloc.currentTheme == ThemeType.light ? [Colors.lightBlue[500], Colors.blue[500]] : [],
//          // whitish to gray
//          tileMode: TileMode.clamp, // repeats the gradient over the canvas
//        )
    );
  }

  static BoxDecoration displayTrackTile(ThemeData theme) {
//    if (ThemeProvider.ofType(theme) == ThemeType.dark) {
//      print('isDark');
    return BoxDecoration(
//      borderRadius: new BorderRadius.all(Radius.circular(8.0)),
      boxShadow: Bloc.bloc.currentTheme == ThemeType.light
          ? [
              BoxShadow(
                color: Colors.grey.withOpacity(0.95),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(0, 1), //
              )
            ]
          : [],
      color: Bloc.bloc.currentTheme == ThemeType.light
          ? Colors.white
          : CustomColors.semiBlack,
//      gradient: LinearGradient(
//        begin: Alignment.topLeft,
//        end: Alignment.bottomRight,
//        // 10% of the width, so there are ten blinds.
//        colors: Bloc.bloc.currentTheme == ThemeType.light ? [Colors.lightBlue[500], Colors.blue[500]] : [],
//        // whitish to gray
//        tileMode: TileMode.clamp, // repeats the gradient over the canvas
//      ),
    );
//    } else {
//      print('isLight');
//    }
  }

  static displayTrackCard(ThemeData theme) {
    if (Bloc.bloc.currentTheme == ThemeType.light) {
      return BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 10.0, // has the effect of softening the shadow
            spreadRadius: 5.0, // has the effect of extending the shadow
            offset: Offset(
              10.0, // horizontal, move right 10
              10.0, // vertical, move down 10
            ),
          )
        ],
        color: theme.backgroundColor,
      );
    } else {
      return BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black,
            spreadRadius: 2,
            blurRadius: 5,
            offset: Offset(0, 1), //
          )
        ],
        gradient: LinearGradient(
            begin: Alignment.bottomRight,
            end: Alignment.topLeft,
            colors: <Color>[theme.primaryColor, CustomColors.semiBlack]),
      );
    }
  }

  static playPanel(ThemeData theme) {
    return BoxDecoration(
//      gradient: LinearGradient(
//          begin: Alignment.topCenter,
//          end: Alignment.bottomCenter,
//          colors: <Color>[CustomColors.semiBlack, CustomColors.background]),
      color: CustomColors.semiBlack,
    );
  }
}
