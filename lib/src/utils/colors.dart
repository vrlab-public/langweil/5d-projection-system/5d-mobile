import 'dart:ui';

import 'package:flutter/material.dart';

/// Static color reference
class CustomColors {
  static final Color darkGrey = Color(0xFF212121);
  static final Color semiBlack = Color(0xFF121212);
  static final Color midGrey = Color(0xFF535353);
  static final Color lightGrey = Color(0xFFb3b3b3);
  static final background = Colors.black;

}