import 'package:flutter/material.dart';
import 'package:projection/src/utils/colors.dart';

/// Theme enum
enum ThemeType {
  light,
  dark,
}

/// Theme provider for configuration
class ThemeProvider {
  static ThemeData dark = darkTheme();
  static ThemeData light = defaultTheme();


  static ThemeData defaultTheme() {
    return ThemeData(
      backgroundColor: Colors.white,
    );
  }

  static ThemeData darkTheme() {
    return ThemeData(
        brightness: Brightness.dark,
        backgroundColor: CustomColors.background,
        primarySwatch: Colors.orange);
  }

  static ThemeType ofType(ThemeData toCheck) {
    if (toCheck == dark) return ThemeType.dark;
    return ThemeType.light;
  }
}
