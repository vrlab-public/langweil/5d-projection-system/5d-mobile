import 'dart:async';

/// Validators for form input
class Validators {
  final validateUsername =
  StreamTransformer<String, String>.fromHandlers(handleData: (username, sink) {
    RegExp exp = new RegExp(r'^[a-zA-Z_0-9]+$');
    if (exp.hasMatch(username)) {
      sink.add(username);
    } else {
      sink.addError('Enter a valid username');
    }
  });

  final validatePassword =
  StreamTransformer<String, String>.fromHandlers(handleData: (password, sink) {
    if (password.length > 3) {
      sink.add(password);
    } else {
      sink.addError('Enter a valid password');
    }
  });
}
