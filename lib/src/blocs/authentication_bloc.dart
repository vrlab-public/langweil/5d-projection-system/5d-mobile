import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:async/async.dart';
import 'package:projection/src/resources/repository.dart';
import 'package:projection/src/screens/login_screen.dart';
import 'package:projection/src/utils/validators.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/material.dart';

import 'bloc.dart';

/// Authentication bloc used for state management
class AuthenticationBloc with Validators {
  final _repository = Repository();
  var _username, _password, _rememberMe, _validating, _authStream;
  bool rememberLogin = false;

  Stream<bool> get authStream => _authStream.stream;

  AuthenticationBloc() {
    initAuth();
  }

  /// Initializes the login form an page
  void initAuth() {
    print('init auth');
    _username = new BehaviorSubject<String>();
    _password = new BehaviorSubject<String>();
    _rememberMe = new BehaviorSubject<bool>();
    changeRememberMe(false);
    _validating = BehaviorSubject<bool>();
    _authStream = StreamController<bool>.broadcast();
    _authStream.stream.listen((publish) {
      print('authBloc listen to stream and $publish');
      Bloc.bloc.setAuthenticated(publish);
    });
  }

  Stream<String> get username => _username.stream.transform(validateUsername);

  Stream<bool> get rememberMe => _rememberMe.stream;

  Stream<String> get password => _password.stream.transform(validatePassword);

  Stream<bool> get submitValid => CombineLatestStream.combine2(
      username, password, (String e, String p) => e.length > 0 && p.length > 0);

  Stream<bool> get isValidating => _validating.stream;

  Function(String) get changeUsername => _username.sink.add;

  Function(String) get changePassword => _password.sink.add;

  Function(bool) get changeRememberMe => _rememberMe.sink.add;

  String get lastUsername => _username.value;

  String get lastPassword => _password.value;

  /// Sequence of connection testing and security token verification with server-backend
  Future<bool> verifyAuthentication() async {
    if (Bloc.bloc.isAuthenticated) return true;
    //test connection
    bool connectionValid =
        await _repository.testConnection().catchError((onError) {
      if (onError is SocketException) {
        handleError(onError);
      }
      return null;
    });
    if(connectionValid == null) return null;
    if(!connectionValid) return false;

    String token = await Bloc.bloc.readAuthorizationToken();
    print('token $token');
    if (token == null) {
      return false;
    }
    bool isTokenValid =
        await _repository.testToken(token).catchError((onError) {
      if (onError is SocketException) {
        handleError(onError);
      } else {
        print('Unknown error $onError');
      }
    });
    if (isTokenValid != null) _authStream.sink.add(isTokenValid);
    if(isTokenValid)  _repository.setAuthToken(token);
    return isTokenValid;
  }

  Future<bool> logout() {
    print('logout');
    Bloc.bloc.deleteAuthorizationToken();
    Bloc.bloc.setAuthenticated(false);
  }

  submit() {
    _validating.sink.add(true);
    final validUsername = _username.value;
    final validPassword = _password.value;
    rememberLogin = _rememberMe.value;
    print('SUBMIT');
    var login = _repository.login(validUsername, validPassword, rememberLogin).then((logged) {
      _validating.sink.add(false);
      _authStream.sink.add(logged);
      print('login $logged');
    }).catchError((onError){
      if (onError is SocketException) {
        handleError(onError);
      }
    });
  }

  void handleError(SocketException error) {
    print('Handle error $error');
    _authStream.sink.addError("Connecting to the exhibition");
  }

  dispose() {
    _username.close();
    _password.close();
    _validating.close();
    _authStream.close();
    _rememberMe.close();
  }
}
