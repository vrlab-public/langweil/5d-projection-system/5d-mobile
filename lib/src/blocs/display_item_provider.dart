import 'package:flutter/material.dart';
import 'display_items_bloc.dart';
//export 'comments_bloc.dart';

class DisplayItemProvider extends InheritedWidget {
  final DisplayItemBloc displayItemBloc;

  DisplayItemProvider({Key key, Widget child})
      : displayItemBloc = new DisplayItemBloc(),
        super(key: key, child: child);


  bool updateShouldNotify(_) => true;

  static DisplayItemBloc of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<DisplayItemProvider>()
        .displayItemBloc;
  }
}
