import 'dart:async';

import 'package:projection/src/models/control_model.dart';
import 'package:projection/src/models/displaytrack_model.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/rxdart.dart' as prefix0;
import '../resources/repository.dart';

/// Enum for acceptable control events
enum ControlEvent { play, pause, forward, rewind, loop, shuffle, stop, goto }

/// Control bloc used for state management and playback related requests
class ControlBloc {
  static const connect_interval = 200;
  static const isPlaying_maxIncompatible =
      40; //when set in app is_playing is different than received from server
  static const isRunning_maxIncompatible =
      3; //when set in app running track is different than received from server
  bool _isPlaying = true;
  bool _isPlayingFromTime = false;
  bool _force = false;
  bool _hasPassedForcedTrack = false;
  bool isShuffle = false;
  bool isLoop = false;

  int isPlayingIncompatibleCounter = 0;
  int isRunningIncompatibleCounter = 0;
  int runningDisplayTrackId = -1;
  List<Track> parsedPlaylist = [];

  Timer iterateControlTimer;

  final _repository = Repository();
  final _playControl = BehaviorSubject<ControlModel>();
  final _playlistStream = BehaviorSubject<List<DisplayTrackModel>>();


  bool get isPlaying => _isPlaying;
  Stream<List<DisplayTrackModel>> get playlist => _playlistStream.stream;
  Stream<ControlModel> get playControl => _playControl.stream;

  Function(List<DisplayTrackModel>) get updatePlaylist =>  _playlistStream.sink.add;
  Function(ControlModel) get updateControl => _playControl.sink.add;




  setPlaying(bool isPlayingFromTime) {
    _isPlayingFromTime = isPlayingFromTime;
    if (_isPlayingFromTime != _isPlaying) {}
  }

  Future<bool> setDisplaySetToPlay(int displaySetId) {
    return _repository.setDisplaySetToPlay(displaySetId);
  }

  Future<bool> runAutonomous(){
    return _repository.runAutonomous();
  }

  Future<bool> setControlEvent(ControlEvent event, {dynamic arguments}) async {
    switch (event) {
      case ControlEvent.play:
        _repository.play().then((response) {
          if (response) _isPlaying = true;
        });
        break;
      case ControlEvent.pause:
        _repository.pause().then((response) {
          if (response) _isPlaying = false;
        });
        break;
      case ControlEvent.stop:
        _repository.stop().then((response) {
          if (response) _isPlaying = false;
        });
        break;
      case ControlEvent.forward:
        _repository.forward().then((response) {});
        break;
      case ControlEvent.rewind:
        _repository.rewind().then((response) {
        });
        break;
      case ControlEvent.loop:
        _repository.loop().then((response) {
          if(response) isLoop = !isLoop;
        });
        break;
      case ControlEvent.shuffle:
        _repository.shuffle().then((response) {
          if(response) isShuffle = !isShuffle;
        });
        break;
      case ControlEvent.goto:
        int currentDisplaySetId = arguments[0];
        int currentPlayedDisplayTrack = int.parse(arguments[1].toString());
        int goingToDisplayTrackId = int.parse(arguments[2].toString());
        _repository
            .goto(currentDisplaySetId,currentPlayedDisplayTrack,goingToDisplayTrackId)
            .then((response) {
          if (response) {
            _force = true;
            _hasPassedForcedTrack = false;
            _isPlaying = true;
            runningDisplayTrackId = goingToDisplayTrackId;
            updatePlaylist(_playlistStream.value);
          }
        });
        break;
    }
  }
  /// Initializes the periodic playback status querying
  startIntervalQuerying() {
    iterateControlTimer = new Timer.periodic(
        Duration(milliseconds: connect_interval),
        (Timer t) => _repository.fetchControl().then((control) {
              verifyThatPlayingCompatible(control);
              int parsedRunningDisplayTrackId =
                  control.playlistFromClient.isEmpty
                      ? -1
                      : control.playlistFromClient
                          .singleWhere((track) => track.running)
                          .displayTrackId;
              if (control.playlistFromClient.isNotEmpty) {
                parsedPlaylist = control.playlistFromClient;
              }
              verifyThatGotoCompatibleWithParsed(parsedRunningDisplayTrackId);
              updateControl(control);
            }));
  }

  void verifyThatPlayingCompatible(ControlModel control) {
    if (control.isPlaying != _isPlaying) {
      isPlayingIncompatibleCounter++;
    } else {
      isPlayingIncompatibleCounter = 0;
    }
    if (isPlayingIncompatibleCounter >= isPlaying_maxIncompatible) {
      _isPlaying = control.isPlaying;
    }
  }


  /// Checks for inconsistencies in playback in the server and in the app
  void verifyThatGotoCompatibleWithParsed(int parsedRunningDisplayTrackId) {
    print(
        'Forced $_force has passed $_hasPassedForcedTrack running $runningDisplayTrackId parsed $parsedRunningDisplayTrackId');
    if (parsedRunningDisplayTrackId != runningDisplayTrackId) {
      if (_force) {
        if (_hasPassedForcedTrack) {
          _force = false;
          _hasPassedForcedTrack = false;
        }
      } else {
        isRunningIncompatibleCounter++;
        print(
            'counter $isRunningIncompatibleCounter >= $isRunning_maxIncompatible');
        if (isRunningIncompatibleCounter >= isRunning_maxIncompatible) {
          print(
              'Running is forced from parsed playlist due to the inconsistency $parsedRunningDisplayTrackId');
          runningDisplayTrackId = parsedRunningDisplayTrackId;
          updatePlaylist(_playlistStream.value);
        }
      }
    } else if (_force) {
      _hasPassedForcedTrack = true;
    } else {
      isRunningIncompatibleCounter = 0;
    }
  }

  stopIntervalQuerying() {
    if (iterateControlTimer != null) iterateControlTimer.cancel();
  }

  ControlBloc() {
    updateControl(ControlModel.empty());
  }

  clearCache() {
    _repository.clearCache();
  }

  dispose() {
    _playlistStream.close();
    _playControl.close();
  }
}
