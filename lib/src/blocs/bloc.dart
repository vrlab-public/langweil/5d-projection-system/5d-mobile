import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:projection/src/utils/themes.dart';
import 'authentication_bloc.dart';
import 'package:rxdart/rxdart.dart';
import '../resources/repository.dart';

/// Main singleton object for state management
class Bloc {

  // serverRoot is a back-end server address with correct opened port
//  static const String serverRoot = 'http://192.168.0.101:8080';
  static const String serverRoot = 'http://192.168.43.218:8080';


  static const String _authorizationTokenKey = "Authorization";
  final AuthenticationBloc _authenticationBloc = AuthenticationBloc();
  final _theme = BehaviorSubject<ThemeType>();
  final _storage = FlutterSecureStorage();
  final _repository = Repository();

  ThemeType _defaultTheme = ThemeType.light;

  Stream<ThemeType> get themeStream => _theme.stream;

  bool _isAuthenticated = false;
  bool isInitialized = false;

  Bloc() {
    setCurrentTheme(_defaultTheme);
  }


  /// Saves theme sets by user
  void setDefaultTheme(ThemeType type){
    _defaultTheme = type;
    setCurrentTheme(type);

  }

  /// Propagates theme changes
  void setCurrentTheme(ThemeType type) {
      _theme.sink.add(type);
  }

  AuthenticationBloc get authenticationBloc => _authenticationBloc;
  ThemeType get currentTheme => _theme.value;
  ThemeType get defaultTheme => _defaultTheme;
  bool get isAuthenticated => _isAuthenticated;

  void setAuthenticated(bool authenticated) {
    _isAuthenticated = authenticated;
  }

  void writeValidAuthorizationToken(String token) {
    _storage.write(key: _authorizationTokenKey, value: token);
    _isAuthenticated = true;
  }

  void deleteAuthorizationToken() {
    _storage.delete(key: _authorizationTokenKey);
  }

  Future<String> readAuthorizationToken() async {
    return await _storage.read(key: _authorizationTokenKey);
  }

  dispose() {
    _theme.close();
  }

  static final bloc = Bloc();
}
