import 'package:flutter/material.dart';
import 'control_bloc.dart';

/// Provider for ControlBloc class
class ControlProvider extends InheritedWidget {
  final ControlBloc bloc;

  ControlProvider({Widget child})
      : bloc = ControlBloc(),
        super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static ControlBloc of(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<ControlProvider>()).bloc;
  }
}
