import 'dart:async';

import 'package:projection/src/models/displayset_model.dart';
import 'package:rxdart/rxdart.dart';
import '../models/displaytrack_model.dart';
import '../resources/repository.dart';

/// Provider for DisplayItemBloc class
class DisplayItemBloc {
  final _repository = Repository();
  final _displaySets = PublishSubject<List<DisplaySetModel>>();
  final _displayTracks = PublishSubject<List<DisplayTrackModel>>();

  Stream<List<DisplaySetModel>> get displaySets => _displaySets.stream;

  Stream<List<DisplayTrackModel>> get displayTracks => _displayTracks.stream;

  DisplayItemBloc() {}

  fetchDisplaySets() async {
    var displaySets = await _repository.fetchDisplaySets();
    _displaySets.sink.add(displaySets);
  }

  dispose() {
    _displaySets.close();
    _displayTracks.close();
  }

  void fetchDisplayTracksOfDisplaySet(int displaySetId) async {
    var displayTracks = await _repository.fetchDisplayTracksOfSet(displaySetId);
    _displayTracks.sink.add(displayTracks);
  }
}
